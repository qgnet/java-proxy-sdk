# java-proxy-sdk

#### 介绍
这是一个关于如何使用青果网络下代理IP产品的sdk包，本仓库为java版本。目前主要包括了动态共享按时，动态共享按量，以及动态独享等三款产品的使用代码示例。具体如何使用sdk请见Test目录下的TestCrawler.java文件。
