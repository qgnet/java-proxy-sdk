package net.qg.proxy;

import java.net.http.HttpResponse;

public interface RequestInterface
{
    /**
     * 请求
     */
    HttpResponse<String> request();
}
