package net.qg.proxy;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import net.qg.proxy.beans.Data;
import net.qg.proxy.utils.Functions;
import net.qg.proxy.utils.Logger;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.net.http.HttpResponse;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.ReentrantLock;

public class IPPool {
    private final ReentrantLock lock;
    private final CountDownLatch readyCountDownLatch;
    private Profile profile;
    private List<Data> pool;

    public IPPool(Profile profile) {
        this.profile = profile;
        this.lock = new ReentrantLock();
        this.readyCountDownLatch = new CountDownLatch(1);
    }

    public void fillIPPool(Request request) throws Exception {
        request.setProfile(profile);
        HttpResponse<String> requestResponse = request.request();
        JSONObject result = new JSONObject(requestResponse.body());

        if (result.getInt("Code") != 0) {
            throw new Exception(result.getString("Msg"));
        }

        Type type = new TypeToken<List<Data>>() {
        }.getType();

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

        lock.lock();
        this.pool = gson.fromJson(result.getJSONArray("Data").toString(), type);
        for (Data data : this.pool) {
            data.setUsable(true);
        }
        lock.unlock();
    }

    public void Collector(Integer period, Request request) {
        new Thread(() -> {
            do {
                try {
                    fillIPPool(request);
                    Logger.info("IP池新增 %d 个IP", pool.size());
                } catch (Exception e) {
                    Logger.error("IP池填充失败: %s", e.getMessage());
                } finally {
                    readyCountDownLatch.countDown();
                    Functions.unsafeSleep(period);
                }
            } while (true);
        }).start();
    }

    public void WaitReady() {
        try {
            readyCountDownLatch.await();
        } catch (InterruptedException e) {
            Logger.error(e.getMessage());
        }
    }

    public Data Get() {
        lock.lock();
        if (pool == null) {
            lock.unlock();
            return null;
        }

        for (Data it : pool) {
            if (!it.getUsable()) {
                continue;
            }

            if (it.getDeadline().getTime() - 3000 < new Date().getTime()) {
                continue;
            }

            lock.unlock();
            // 使用过一次的不再继续使用
            it.setUsable(false);
            return it;
        }

        lock.unlock();
        return null;
    }

    public interface Function {
        void run(Data data);
    }

    public void Range(Function func) {
        lock.lock();
        for (Data it : pool) {
            func.run(it);
        }
        lock.unlock();
    }
}
