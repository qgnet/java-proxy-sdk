package net.qg.proxy.crawler;

import net.qg.proxy.IPPool;
import net.qg.proxy.beans.Option;
import net.qg.proxy.beans.Runnable;
import okhttp3.Credentials;

import java.net.Proxy;

public class Options {
    public static Option withProtocol(Proxy.Type proto) {
        return (Crawler crawler) -> {
            crawler.setProto(proto);
        };
    }

    public static Option withIPPool(IPPool pool) {
        return (Crawler crawler) -> {
            crawler.setPool(pool);
        };
    }

    public static Option withBasicAuth(String name, String password) {
        return (Crawler crawler) -> crawler.setAuthenticator((route, response) -> {
            String credential = Credentials.basic(name, password);
            return response.request().newBuilder().header("Proxy-Authorization", credential).build();
        });
    }

    public static Option withRunnable(Runnable func) {
        return (Crawler crawler) -> {
            crawler.setRunnable(func);
        };
    }
}
