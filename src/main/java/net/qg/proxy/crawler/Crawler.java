package net.qg.proxy.crawler;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.concurrent.CountDownLatch;

import net.qg.proxy.IPPool;
import net.qg.proxy.beans.Data;
import net.qg.proxy.beans.Option;
import net.qg.proxy.beans.Runnable;
import net.qg.proxy.sdk.DynamicSharingTime;
import okhttp3.Authenticator;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Crawler {
    private IPPool pool;
    private Runnable runnable;
    private Proxy.Type proto;
    private Authenticator authenticator;

    public Crawler(Option... options) {
        for (Option o : options) {
            o.run(this);
        }
    }

    public void start() {
        new Thread(() -> {
            while (true) {
                Data ip = pool.Get();
                if (ip == null) {
                    continue;
                }

                Proxy proxy = new Proxy(this.proto, new InetSocketAddress(ip.IP, Integer.parseInt(ip.port)));
                OkHttpClient client = new OkHttpClient.Builder().
                        proxy(proxy).
                        proxyAuthenticator(authenticator).
                        build();
                if (!this.runnable.run(ip, client)) {
                    break;
                }
            }
        }).start();
    }

    public void setPool(IPPool pool) {
        this.pool = pool;
    }

    public void setProto(Proxy.Type proto) {
        this.proto = proto;
    }

    public void setAuthenticator(Authenticator authenticator) {
        this.authenticator = authenticator;
    }

    public void setRunnable(Runnable runnable) {
        this.runnable = runnable;
    }
}
