package net.qg.proxy;

public abstract class Request implements RequestInterface
{
    private Profile profile;

    public void setProfile(Profile profile)
    {
        this.profile = profile;
    }

    public Profile getProfile()
    {
        return profile;
    }
}
