package net.qg.proxy;

import java.net.URI;

public class Profile {
    private String key = "";
    private String pwd = "";
    private Integer num = 1;

    public static Profile with(String key, String pwd, Integer num) {
        Profile profile = new Profile();
        profile.key = key;
        profile.pwd = pwd;
        profile.num = num;
        return profile;
    }

    public static Profile empty() {
        return with("", "", 0);
    }

    public String buildQueryString() {
        return String.format("Key=%s&Pwd=%s&Num=%d", key, pwd, num);
    }

    public URI buildURI(String prefix) {
        return java.net.URI.create(String.format("%s?%s", prefix, buildQueryString()));
    }
}
