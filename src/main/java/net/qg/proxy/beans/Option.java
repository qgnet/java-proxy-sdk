package net.qg.proxy.beans;

import net.qg.proxy.crawler.Crawler;

public interface Option {
    void run(Crawler crawler);
}
