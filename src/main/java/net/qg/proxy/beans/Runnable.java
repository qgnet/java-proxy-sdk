package net.qg.proxy.beans;

import okhttp3.OkHttpClient;

public interface Runnable {
    Boolean run(Data data, OkHttpClient client);
}
