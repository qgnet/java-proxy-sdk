package net.qg.proxy.beans;

import java.util.Date;

public class Data {
    public String IP;
    public String port;
    public Date deadline;
    public String host;
    public boolean usable;

    public boolean getUsable() {
        return usable;
    }

    public void setUsable(boolean usable) {
        this.usable = usable;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    @Override
    public String toString() {
        return "Data{" +
                "IP='" + IP + '\'' +
                ", port='" + port + '\'' +
                ", deadline=" + deadline +
                ", host='" + host + '\'' +
                ", usable=" + usable +
                '}';
    }
}
