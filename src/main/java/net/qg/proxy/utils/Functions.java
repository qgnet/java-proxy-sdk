package net.qg.proxy.utils;

public class Functions {
    static public void unsafeSleep(long s) {
        try {
            Thread.sleep(s);
        } catch (InterruptedException e) {
            Logger.error(e.getMessage());
        }
    }
}
