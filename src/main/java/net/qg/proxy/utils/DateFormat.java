package net.qg.proxy.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormat {
    static SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    public static String format(Date date) {
        return timeFormat.format(date);
    }

    public static String now() {
        return format(new Date());
    }
}
