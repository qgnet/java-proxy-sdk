package net.qg.proxy.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;

public class Logger {
    public static void info(String format, Object... args) {
        Formatter fmt = new Formatter().format(format, args);
        System.out.printf("INFO | %s : %s\n", DateFormat.now(), fmt);
    }

    public static void error(String format, Object... args) {
        Formatter fmt = new Formatter().format(format, args);
        System.out.printf("ERROR | %s : %s\n", DateFormat.now(), fmt);
    }
}
