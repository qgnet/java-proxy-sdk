package net.qg.proxy.sdk;


import net.qg.proxy.Request;
import net.qg.proxy.utils.HTTPClient;

import java.net.URI;
import java.net.http.HttpResponse;

public class DynamicExclusive extends Request {
    public HttpResponse<String> request() {
        var profile = this.getProfile();
        URI uri = profile.buildURI("https://proxy.qg.net/replace");

        return HTTPClient.get(uri);
    }
}
