import net.qg.proxy.IPPool;
import net.qg.proxy.Profile;
import net.qg.proxy.beans.Data;
import net.qg.proxy.crawler.Crawler;
import net.qg.proxy.crawler.Options;
import net.qg.proxy.sdk.DynamicSharingTime;
import net.qg.proxy.utils.Functions;
import net.qg.proxy.utils.Logger;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;

public class TestCrawler {
    private String authkey = "765104B1";
    private String pwd = "177070DD5343";
    private Integer num = 100;

    @Test
    public void testCrawler() {
        IPPool pool = new IPPool(Profile.with(authkey, pwd, num));
        pool.Collector(1000 * 60, new DynamicSharingTime());
        pool.WaitReady();

        new Crawler(
                Options.withProtocol(Proxy.Type.HTTP),
                Options.withIPPool(pool),
                Options.withBasicAuth(authkey, pwd),
                Options.withRunnable((Data data, OkHttpClient client) -> {
                    try {
                        Request request = new Request.Builder().url("https://ip.cn/api/index?type=0").get().build();
                        Response response = client.newCall(request).execute();
                        Logger.info(response.body().string());
                    } catch (IOException e) {
                        Logger.error("use ip: %s, error: %s", data.host, e.getMessage());
                    }

                    Functions.unsafeSleep(1000);
                    return true;
                })).start();

        Functions.unsafeSleep(100000000000L);
    }
}
