import net.qg.proxy.IPPool;
import net.qg.proxy.Profile;
import net.qg.proxy.beans.Data;
import net.qg.proxy.sdk.DynamicExclusive;
import net.qg.proxy.sdk.DynamicSharing;
import net.qg.proxy.sdk.DynamicSharingTime;
import net.qg.proxy.utils.DateFormat;
import net.qg.proxy.utils.Functions;
import net.qg.proxy.utils.Logger;
import org.junit.jupiter.api.Test;


class DynamicSharingTimeTest {
    @Test
    void test() {
        IPPool pool = new IPPool(Profile.with("D289B918", "28C81C3A15D4", 20));
        pool.Collector(1000 * 62, new DynamicSharingTime());
        pool.WaitReady();

        while (true) {
            Data ip = pool.Get();
            if (ip == null) {
                continue;
            }

            Logger.info("获取到按时动态共享可用IP: %-21s，到期时间: %s", ip.getHost(), DateFormat.format(ip.getDeadline()));
            Functions.unsafeSleep(3000);
        }
    }
}

class DynamicSharingTest {
    @Test
    void test() {
        IPPool pool = new IPPool(Profile.with("D289B918", "28C81C3A15D4", 20));
        pool.Collector(1000 * 62, new DynamicSharing());
        pool.WaitReady();

        while (true) {
            Data ip = pool.Get();
            if (ip == null) {
                continue;
            }

            Logger.info("获取到动态共享可用IP: %-21s，到期时间: %s", ip.getHost(), DateFormat.format(ip.getDeadline()));
            Functions.unsafeSleep(3000);
        }
    }
}

class DynamicExclusiveTest {
    @Test
    void test() {
        IPPool pool = new IPPool(Profile.with("D289B918", "28C81C3A15D4", 20));
        pool.Collector(1000 * 62, new DynamicExclusive());
        pool.WaitReady();

        while (true) {
            Data ip = pool.Get();
            if (ip == null) {
                continue;
            }

            Logger.info("获取到动态独享可用IP: %-21s，到期时间: %s", ip.getHost(), DateFormat.format(ip.getDeadline()));
            Functions.unsafeSleep(3000);
        }
    }
}
